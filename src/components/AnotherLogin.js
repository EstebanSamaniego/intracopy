import React, { Component } from 'react'
import { Grid, Paper, TextField, Button, Typography, Link } from "@material-ui/core"
import VisibilityOffOutlinedIcon from '@material-ui/icons/VisibilityOffOutlined';

import {
    Redirect
} from 'react-router-dom';

class AnotherLogin extends Component {

    constructor(props) {
        super(props)
    
        this.state = {
             username: '',
             password: '',
             showUsernameField: true
        }
    }
    
    render() {
        const paperStyle = {
            backgroundColor: '#214565',
            height: '100vh'
        }
    
        const paperStyle2 = {
            
            height: '100%',
            width: 280,
            margin: '20px auto',
            padding: 20
        }
    
        const paperStyle3 = {
            backgroundColor: '#214565',
            height: '100vh',
            width: '35vw'
        }
    
        const paperStyle4 = {
            height: '100vh',
            padding: 20,
            backgroundColor: '#214565'
        }

        const paperStyle5 = {
            height: '100vh',
            padding: 20
        }
    
        const btnstyle={
            margin:'8px 0',
            width: 280,
            margin: '20px auto'
        }

        const bannerHeader = {
            padding: 50
        }

        const bannerFooter = {
            fontSize: '14px',
            color: '#f5eeee'
        }

        const footerStyle = {
            fontSize: '12px',
            color: '#d4c6c6'
        }

        const mainFooter = {
            padding: 30
        }


        this.setUsername = (e) => {
            this.setState({
                ...this.state,
                username: e.target.value
            })
        }

        this.setPassword = (e) => {
            this.setState({
                ...this.state,
                password: e.target.value
            })
        }

        this.submitForm = () => {
            if(this.state.showUsernameField){
                this.setState({
                    ...this.state,
                    showUsernameField: false,
                    password: ''
                })

                console.log("State: ");
                console.log(this.state);
            }else{
                console.log("State: ");
                console.log(this.state);
            }

            //return <Redirect to="/home/" />
            this.props.history.push('/home/');
        }

        this.redirectToHelp = () => {
            console.log("Redirecting to help page...");
        }

        
        return (
            <Grid container direction="row" justify="center" alignItems="center">
                <Grid item xs={4}>

                    <Grid container style={paperStyle4} direction="column" justify="space-between" alignItems="center" >
                        <Grid item style={bannerHeader}><h2>TCS | TATA Consultancy Services</h2></Grid>
                        <Grid item ></Grid>
                        <Grid item style={bannerFooter}>
                            <Link href="#" onClick={this.redirectToHelp} color="inherit">Terms of Use</Link> |
                            <Link href="#" onClick={this.redirectToHelp} color="inherit"> Browser and display compatibility</Link><br/>
                            
                            <Typography style={footerStyle}>
                                Copyright @ 2021 Tata Consultancy Services<br/>
                                Entry to this site is restricted to employees and affiliates
                            </Typography>
                        </Grid>
                    </Grid>

                </Grid>

                <Grid item xs={8}>
                    
                    <Grid container style={paperStyle5} direction="column" justify="space-between" alignItems="center" >
                        <Grid item style={bannerHeader}>
                            Ultimatix<br/>
                            Where ALL the action is!
                        </Grid>
                        
                        <Grid item >
                            <Grid container direction="row" justify="center" alignItems="center" >
                                

                                { this.state.showUsernameField ? 
                                    <TextField  label='Username' placeholder='Enter username' required 
                                                onChange={this.setUsername} />
                                    :
                                    null
                                }

                                { !this.state.showUsernameField ? 
                                    <TextField label='Password' type="password" required 
                                        onChange={this.setPassword} />
                                    :
                                    null
                                }

                                { this.state.showUsernameField ? <VisibilityOffOutlinedIcon/> : null }
                            </Grid>
                        </Grid>

                        <Grid item>
                            <Grid container direction="row" justify="center" alignItems="center" style={mainFooter}>
                                <Grid item xs={12} >
                                    <Button type='button' color='primary' variant="contained" style={btnstyle} fullWidth onClick={this.submitForm}>
                                        Proceed
                                    </Button>
                                </Grid>
                                
                                <Grid item xs={12}>
                                    <Typography >
                                        <Link href="#" onClick={this.redirectToHelp}>
                                            Need Help?
                                        </Link>
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    
                </Grid>

            </Grid>
        )
    }
    
}

export default AnotherLogin;