import logo from './logo.svg';
import './App.css';
import Login from './components/Login';
import AnotherLogin from './components/AnotherLogin';
import Home from './components/Home';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <header className="AppHeader">
          <Switch>
            <Route exact path="/" component={AnotherLogin}></Route>
            <Route exact path="/home/" component={Home}></Route>
          </Switch>
        </header>
      </div>

     
    </Router>
  );
}

export default App;
